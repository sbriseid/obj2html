from obj2html import obj2html

# run in the root folder, not inside exaples, to have the relative path working
obj2html('test/assets/model.obj', 'index.html')

# open index.html on your favourite browser (if the model is large it can take more times)
